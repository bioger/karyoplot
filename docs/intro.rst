About Karyoplot
===============

What is Karyoplot ?
-------------------

Karyoplot is a python package developed to draw a karyoplot image when given specific data.
With a Fasta File and BED Files, karyoplot will return a visual representation of a genome along with the features given in the BED file data.


How does it work ?
------------------

Karyoplot takes as an argument a configuration file with different sections which will have a reference to the Fasta File and sections for each BED File
representing a feature, along with an output destination for the final image.
It then takes those information and using different python module and libraries such as matplotlib and pysam is able to create subplots and thus having a plot for each sequence of a genome with the associated features at their precise position.

Process
~~~~~~~


.. graphviz::
   :name: Process
   :align: center

   digraph process {
      "Parse sections and options from configuration file" -> "Selects chromosomes to draw";
      "Selects chromosomes to draw" -> "Creates Section/Chromosome Objects";
      "Creates Section/Chromosome Objects" -> "Builds plot for every feature and every chromosome";
      "Builds plot for every feature and every chromosome" -> "Track datatype option is BED";
      "Builds plot for every feature and every chromosome" -> "Track graph_type option is GC";
      "Track datatype option is BED" -> "Fetches bed file feature positions";
      "Track graph_type option is GC" -> "Calculates GC content";
      "Calculates GC content" -> "Draws plot";
      "Fetches bed file feature positions" -> "Draws plot";
   }

To start off, a good configuration file is needed, you can learn how to set your :ref:`options <config_file>`.