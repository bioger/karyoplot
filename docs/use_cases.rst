Use Cases
===============

.. _use_cases:

Data preparation
----------------

Currently only sorted, compressed and indexed bed files are supported. To prepare the data files, follow this process:

.. code-block:: none

   # sort by name and coordinates
   sort -k1,1 -k2,2n features.bed > features.sort.bed

   # compress
   bgzip features.sort.bed

   # index
   tabix -p bed features.sort.bed.gz

If you want to transform your gff/gtf file in bed:

.. code-block:: none

   # extract coordinates and switch to 0-index 
   grep -v -P "^#" features.gff | awk '{print $1"\t"$4-1"\t"$5}' | sort -k1,1 -k2,2n > features.sort.bed

Test data, example
------------------

Using these 2 files `[fasta] <https://forgemia.inra.fr/bioger/karyoplot/-/raw/main/test-data/SE15195.fasta?ref_type=heads&inline=false>`_ and `[bed] <https://forgemia.inra.fr/bioger/karyoplot/-/raw/main/test-data/SE15195_chromosomes_genes.bed.gz?ref_type=heads&inline=false>`_ , see below some examples of how to use your configuration file to adjust your karyoplot to your convenience.

.. code-block:: none

    # run 
    karyoplot config.ini -o genome_karyo.png -v 2

A karyoplot with no features
----------------------------

With a zoom :
~~~~~~~~~~~

.. code-block:: none

    [canvas]
    title = My Karyoplot
    background_color = white
    width = 30
    height = 25
    constrained_layout = True
    title_fontsize = 30

    [sequence]
    reference = $HOME/user/chromosome.fasta
    color = blue
    size = 9
    zoom = chr_2:9000-99000,chr_7:400000-8000000,chr_3:85250-9500000
    scale = True
    seq_fontsize = 20
    x_label_fontsize = 20

Resulting Image :

.. image:: ../img/zoom_karyoplot.png
  :alt: Simple Karyoplot

Without a zoom :
~~~~~~~~~~~~~~

.. code-block:: none

    [canvas]
    title = My Karyoplot
    background_color = white
    width = 30
    height = 25
    constrained_layout = True
    title_fontsize = 30

    [sequence]
    reference = $HOME/user/chromosome.fasta
    color = black
    size = 9
    limitesize = 50000
    scale = False
    seq_fontsize = 20
    x_label_fontsize = 20

Resulting Image :

.. image:: ../img/plain_karyoplot.png
  :alt: Zoom Karyoplot


A karyoplot with a bed file feature
-----------------------------------

.. code-block:: none

    [canvas]
    title = My Karyoplot
    background_color = white
    width = 30
    height = 25
    constrained_layout = True
    title_fontsize = 30

    [sequence]
    reference = $HOME/user/chromosome.fasta
    # Either define the track color (default color)
    color = red
    # Either enable use of color(s) indicated in the 5th column of the bed file (red, blue, green, ...), if available, else default color
    colorin = True
    size = 9
    limitesize = 2000000
    scale = True
    seq_fontsize = 18
    x_label_fontsize = 25

    [track_gene]
    color = red
    title = gene feature
    type = none
    size = 3
    thickness = 400
    data = $HOME/user/chromosomes_genes.bed.gz
    datatype= Bed
    background_color = lightgrey



Resulting Image :

.. image:: ../img/feature_karyoplot.png
  :alt: Simple Karyoplot


A karyoplot with a gc type feature
----------------------------------

.. code-block:: none

    [canvas]
    title = My karyoplot
    background_color = white
    width = 30
    height = 25
    constrained_layout = True
    title_fontsize = 30

    [sequence]
    reference = $HOME/user/chromosome.fasta
    color = black
    size = 9
    limitesize = 4000000
    scale = True
    seq_fontsize = 20
    x_label_fontsize = 20

    [track_gc]
    color = green
    title = gc feature
    graph_type = GC
    gc_window_length=1000
    gc_window_overlap=0
    thickness = 0.5
    size = 8
    background_color = white
    scaley = True
    y_label_fontsize = 15

Resulting Image :

.. image:: ../img/gc_karyoplot.png
  :alt: Feature Karyoplot

A typical karyoplot
-------------------
When combining both of these configuration files we get the following :

.. code-block:: none

    [canvas]
    title = Final Karyoplot
    background_color = white
    width = 30
    height = 25
    constrained_layout = True
    title_fontsize = 30

    [sequence]
    reference = $HOME/user/chromosome.fasta
    color = black
    size = 9
    zoom = chr_1:10000-10000000,chr_7:400000-8000000,chr_3:85250-9500000
    scale = True
    seq_fontsize = 14
    x_label_fontsize = 20

    [track_gene]
    color = red
    title = gene feature
    type = none
    size = 3
    thickness = 400
    data = $HOME/user/chromosomes_genes.bed.gz
    datatype= Bed
    background_color = lightgrey

    [track_gc]
    color = green
    title = gc feature
    graph_type = GC
    gc_window_length=1000
    gc_window_overlap=0
    thickness = 0.5
    size = 8
    background_color = beige
    scaley = True
    y_label_fontsize = 18

Resulting Image :

.. image:: ../img/typical_karyoplot.png
  :alt: Typical Karyoplot


The feature tracks will appear in the order it shows up in the configuration file, in the previous example the "gene" track appears before the "gc" track.
