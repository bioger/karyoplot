Installing karyoplot 
=====================

It is recommended to use virtual environnements to facilitate dependencies install. To do this:


.. code:: console

  # create your virtual env in a dedicated directory
  python3 -m venv $HOME/myvenvs/karyoplot

  # set up the new env
  source $HOME/myvenvs/karyoplot/bin/activate

  # install karyoplot 
  pip install karyoplot 

  # enjoy
  karyoplot -h 

  # default usage 
  karyoplot config.ini -o genome_karyo.png -v 2

  # to leave the env
  deactivate

