Configuration file
==================

.. _config_file:

Here are some recommendations to set up your configuration file. A configuration file must contain at least 2 sections: `sequence` and `canvas` with optional and mandatory options. The canvas section contains the general graphical settings for the canvas and the sequence section refers to the reference fasta file and the associated drawing options.

canvas
~~~~~~

==================      ===========================================   =======   ============ =========
Option                  Description                                   Type      Requirements Default
==================      ===========================================   =======   ============ =========
title                   Title of the karyoplot                        string    Optional     None
background_color        Sets the karyoplot's background color         string    Optional     white
width                   Sets the width dimension of the canvas        integer   Optional     10
height                  Sets the height dimension of the canvas       integer   Optional     10
constrained_layout      Ensures the good layout of the final image    boolean   Optional     False
title_fontsize          Font size for the Canvas' title               integer   Optional     20
==================      ===========================================   =======   ============ =========

sequence
~~~~~~~~

================      =====================================================================   ====================  ============= =======
Option                Description                                                             Type                  Requirements  Default
================      =====================================================================   ====================  ============= =======
reference             FASTA file                                                              .fasta or .fasta.gz   Mandatory     None
color                 Chromosome color on the plot                                            string                Optional      black
limitesize            Minimum size for a chromosome to be shown in the karyoplot              integer               Optional      50
size                  Size of the plot                                                        integer               Optional      1
zoom                  List of names of chromosomes followed by the positions for the zoom     string                Optional      None
scale                 Shows each chromosome's plot scale(sci notation)                        boolean               Optional      False
seq_fontsize          Font size for the chromosomes' titles                                   integer               Optional      14
x_label_fontsize      Font size for the chromosomes' x labels                                 integer               Optional      20
================      =====================================================================   ====================  ============= =======

If the zoom option is specified, the limitesize parameter is no longer taken into account.

With only these two sections, you will obtain an image with the chromosomes.

To add features to the chromosomes, new sections must be added. A track section is required `for each feature`.
A feature's section must always start by *track_* and finish with anything but a space. Each track's name must be `unique`.

track
~~~~~

====================      ===========================================   =========  ============= =======
Option                    Description                                   Type       Requirements  Default
====================      ===========================================   =========  ============= =======
title                     Title for the feature                         string     Optional      None
color                     Color for the feature                         string     Optional      red
colorin                   Use colors in the 5th column of bed file      boolean    Optional      False 
size                      Size of the feature's plot                    integer    Optional      1
data                      BED file with the feature information         .bed.gz    Mandatory     None
datatype                  The type of file input in Data                bed        Mandatory     None
thickness                 The width of the feature's plot               integer    Optional      1
background_color          The background color for the plot             string     Optional      white
show_small_features       Resize small features to allow display        boolean    Optional      False
====================      ===========================================   =========  ============= =======


Some feature may require additional functions such as calculating a percentage for instance.
In that case, new options are added to track to recognize those features.


=================     ===========================================   =======   ============= =======
Option                Description                                   Type      Requirements  Default
=================     ===========================================   =======   ============= =======
graph_type            The type of graph needed                      gc        Mandatory     None
GC_window_length      Sliding window length                         integer   Optional      1000
GC_window_overlap     Sliding window overlap length                 integer   Optional      100
scaley                Shows the plot's y axis scale(sci notation)   boolean   Optional      False
y_label_fontsize      Font size for the track's y labels            integer   Optional      20
=================     ===========================================   =======   ============= =======

.. note::
    In this current version only type ``gc`` is provided as predefined graph. In this case, no data file is expected. Further improvements will allow different type of graphs for features: raw, density, ....


To get a better idea of how these configuration files work, take a look at the :ref:`use_cases <use_cases>`.

