Welcome to karyoplot documentation!
======================================

:Author: Rosanne Phebe, Nicolas Lapalu and Adeline Simon - `BioinfoBioger platform <https://eng-bioger.versailles-saclay.hub.inrae.fr/teams/internal-platforms/bioinformatics>`_
:Date: |today|
:Version: |version|


.. toctree::
   :hidden:

   install.rst
   options.rst
   use_cases.rst


What is karyoplot ?
-------------------

``karyoplot`` is a python package developed to draw simple karyoplot/ideogram with a simple configuration file like Circos [#f1]_.

How does it work ?
------------------

``karyoplot`` takes as argument a configuration file with different sections based on a sequence Fasta file as reference for ideograms. Each track section refers to a feature file and includes drawing options. ``karyoplot`` is mainly based on ``matplotlib`` for graph rendering and ``pysam`` for bioinformatics file format parsing.

The configuration file `config.ini` is readable by the ``configparser`` Python module. See the :ref:`options <config_file>` available to set up this file.

``karyoplot`` is under developement, currently supported formats are:

* fasta
* bed

available soon:

* gff (currently, switch to bed with provided commands, see `usecases <use_cases>`
* vcf
* bam
* wig/bigWig
   
License
-------

licensed under GNU General Public License v3.0 

Support
-------

For any request please contact nicolas.lapalu[at]inrae.fr or adeline.simon[at]inrae.fr

References
----------

.. [#f1] Krzywinski, M. et al. Circos: an Information Aesthetic for Comparative Genomics. Genome Res (2009) 19:1639-1645
